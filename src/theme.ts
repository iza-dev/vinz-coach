import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
    styles: {
        global: {
            body: {
                bg: "black",
                color: "white"
            }
        }
    },
    colors: {
        comat: "#E4CC37",
        revilal_blue: "#144BB8",
        gobios: "#F8FFF1",
        colint: "#001114",
        dealy: "#213D01",
    },
    sizes: {
        textWidth: '70vmin'
    },
    fonts: {
        heading: "'Bebas Neue', sans-serif",
        body: "'Inter', sans-serif",
        p_intro: "'Roboto', sans-serif"
    }
});

export default theme;
