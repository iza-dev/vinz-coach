import { createBrowserRouter } from 'react-router-dom';
import App from './App';
import { ArticlePage } from './components/articles';
import { HomePage } from './components/homePage';
import { Program, ProgramsListPage } from './components/programs';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: '/',
        element: <HomePage />
      },
      {
        path: 'programs',
        element: <ProgramsListPage />
      },
      {
        path: 'programs/:id',
        element: <Program />
      },
      {
        path: 'articles/entraînements-et-techniques',
        element: <ArticlePage />,
        children: [{ path: ':id', element: <ArticlePage /> }]
      }
    ]
  }
]);
