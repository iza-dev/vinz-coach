import { Box } from '@chakra-ui/react';
import { Outlet } from 'react-router-dom';
import './App.css';
import { Footer } from './components/footer/Footer';
import { Header } from './components/header';

function App() {
  return (
    <Box p={'1rem'}>
      <Header />
      <Outlet />
      <Footer />
    </Box>
  );
}

export default App;
