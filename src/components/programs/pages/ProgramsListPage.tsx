import { Box, Grid, GridItem, Image, Link, Text } from '@chakra-ui/react';
import { FC } from 'react';
import programs from '../../../data/programs.json';

import img from '../../../assets/images/programs/titan_man.png';

export const ProgramsListPage: FC = () => {
  const offset = '2rem';
  return (
    <Grid
      gridTemplateColumns={{
        md: 'repeat(auto-fill, minmax(250px, 1fr) )',
        lg: 'repeat(auto-fill, minmax(400px, 1fr) )'
      }}
      rowGap={{ base: '12rem', lg: '5rem' }}
      columnGap={{ base: '6rem', lg: 0 }}
      width={'90vw'}
      justifyItems={'center'}
      marginX={'auto'}
      marginTop={'10rem'}
    >
      {programs.data.map(program => (
        <GridItem key={program.id} marginLeft={offset} position={'relative'}>
          <Box p="6" width={'250px'} height="350px" bgColor={'red.600'}>
            <Text
              textTransform={'uppercase'}
              fontWeight={'bold'}
              position={'absolute'}
              right={0}
              style={{
                writingMode: 'vertical-rl',
                textOrientation: 'mixed',
                transform: 'scale(-1)'
              }}
            >
              {program.name}
            </Text>
            <Text bottom={0} position={'absolute'}>
              {`Durée ${program.duration_months} mois`}
            </Text>
          </Box>
          <Link href={`/programs/${program.id}`}>
            <Box
              top={`-${offset}`}
              left={`-${offset}`}
              width={'250px'}
              height="350px"
              position="absolute"
              overflow="hidden"
            >
              <Image
                src={img}
                alt={`Body Building Class ${program.name}`}
                width="calc(100% + 20px)"
                height="auto"
                transition="transform 0.3s ease-in-out"
                _hover={{
                  transform: 'scale(1.1)'
                }}
              />
            </Box>
          </Link>
        </GridItem>
      ))}
    </Grid>
  );
};
