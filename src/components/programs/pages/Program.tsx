import {
  Box,
  Button,
  Flex,
  Heading,
  Image,
  List,
  ListIcon,
  ListItem,
  OrderedList,
  Text,
  useTheme
} from '@chakra-ui/react';
import { FC } from 'react';
import { useParams } from 'react-router-dom';

import programs from '../../../data/programs.json';

import { MdCheckCircle } from 'react-icons/md';
import { RxCross2 } from 'react-icons/rx';
import colmar_vinz from '../../../assets/images/programs/colmar_vinz.webp';
import freedom_woman from '../../../assets/images/programs/freedom_woman.webp';
import strength_man from '../../../assets/images/programs/strength_man.webp';
import titan_man from '../../../assets/images/programs/titan_man.webp';
import titan_woman from '../../../assets/images/programs/titan_woman.webp';

export const Program: FC = () => {
  const { id } = useParams<{ id: string }>();
  const theme = useTheme();

  const imgProgramms = [strength_man, freedom_woman, titan_man, titan_woman, colmar_vinz];
  const programId = Number(id) || 1;
  const currentProgram = programs.data[programId - 1];
  return (
    <Box marginX={'auto'} width={theme.sizes.textWidth}>
      <Heading marginX={'auto'} backgroundColor={'red.600'} marginTop="10rem" textAlign={'center'}>
        program {currentProgram.name}
      </Heading>

      <Text mt={'2rem'} textAlign={'justify'}>
        {currentProgram.introduction}
      </Text>

      <Image mt={'2rem'} src={imgProgramms[programId - 1]} marginX={'auto'} />

      <List spacing={3} mt={'2rem'}>
        {currentProgram.content.map(({ name, value }) => (
          <ListItem key={name}>
            <ListIcon
              as={value ? MdCheckCircle : RxCross2}
              color={value ? 'green.500' : 'red.600'}
            />
            {name}
          </ListItem>
        ))}
      </List>

      <Flex mt={'2rem '} gap={'4rem'} alignItems={'center'}>
        <Box>
          <Text>Prix:</Text>
          <Heading>{`${currentProgram.price} €`}</Heading>
        </Box>
        <Button minW={{ base: '5rem', md: '10rem' }} backgroundColor={'red.600'} color={'white'}>
          Acheter
        </Button>
      </Flex>

      <OrderedList mt={'2rem'}>
        {currentProgram.steps.map(({ name, description }) => (
          <ListItem key={name} marginY={'1rem'}>
            {description}
          </ListItem>
        ))}
      </OrderedList>
    </Box>
  );
};
