import { Box, Heading, Image, Link, ListItem, OrderedList, Text, useTheme } from '@chakra-ui/react';
import { FC } from 'react';
import image_1 from '../../../assets/images/article_1.jpeg';
import { hexToRGBA } from '../../../utils/hexToRGBA';

export const ArticlePage: FC = () => {
  const theme = useTheme();

  return (
    <>
      <Box position="relative">
        <Box
          position="absolute"
          top={0}
          left={0}
          width="100%"
          height="70vh"
          backgroundColor={`${hexToRGBA(theme.colors.dealy, 0.5)}`}
          zIndex={-1}
        />
        <Box
          zIndex={10}
          marginX={'auto'}
          width={theme.sizes.textWidth}
          textAlign={'center'}
          paddingTop={'5rem'}
        >
          <Heading as={'h1'} color={theme.colors.gobios} mb={'2rem'}>
            <Box
              as={'span'}
              display="block"
              fontSize={{ base: '4xl', md: '5xl', lg: '6xl' }}
              letterSpacing={{ base: '0', md: '0.3rem', lg: '0.5rem' }}
            >
              Les Fondamentaux de l'Hypertrophie
            </Box>
            <br />
            Construire du Muscle Efficacement
          </Heading>
          <Image
            maxW="95vmin"
            marginLeft={'-12.5vmin'}
            marginY={'5rem'}
            src={image_1}
            alt={'Green double couch with wooden legs'}
          />
          <Text
            fontFamily={theme.fonts.p_intro}
            fontStyle={'italic'}
            fontWeight={'medium'}
            fontSize="xl"
            mb="4rem"
            textAlign={'justify'}
          >
            Vous vous demandez pourquoi, malgré tous vos efforts à la salle, les résultats ne sont
            pas toujours à la hauteur de vos attentes ? La réponse pourrait résider dans la
            compréhension de l'hypertrophie musculaire, le processus biologique au cœur de la
            croissance musculaire. <br />
            #CardioAJeun #EnergieMatinale #VinzerFit #DéfiFitness #VinzerfitVibes
          </Text>
          <Heading mb="4rem" as={'h2'}>
            Qu'est-ce que l'hypertrophie musculaire ?
          </Heading>
          <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
            L'hypertrophie musculaire, bien plus qu'un simple gain de volume, est le fruit d'une
            adaptation biologique de vos muscles au stress imposé par l'entraînement en résistance.
            C'est lorsque les fibres musculaires endommagées se réparent et se renforcent que le
            muscle se développe, non seulement en taille mais aussi en force. <br />
            Mais transformer son physique ne s'arrête pas à soulever des poids. Derrière chaque
            progression, il y a une science : une alchimie précise entre entraînement ciblé,
            nutrition adaptée, repos suffisant et récupération optimisée. Chaque élément est un
            pilier sur lequel repose votre succès. Maximiser l'efficacité de vos entraînements.
            <br /> Pour que chaque séance compte et vous rapproche de votre objectif, une
            compréhension approfondie des principes de l'hypertrophie s'impose. Ce guide est là pour
            vous éclairer sur ces fondamentaux, vous permettant d'élaborer un plan d'action cohérent
            et efficace pour stimuler votre croissance musculaire. <br />
            Découvrons ensemble à travers cet article, comment transformer l'effort en résultats
            palpables En plongeant au cœur des mécanismes de l'hypertrophie musculaire.
          </Text>
          <Heading mb="4rem" as={'h3'}>
            Plus en détails
          </Heading>
          <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
            L'hypertrophie musculaire, terme qui peut paraître complexe, désigne en réalité
            simplement l'augmentation de la taille des muscles. Ce processus est déclenché par des
            séances d'entraînement en résistance, qui ne se contentent pas d'élargir le volume des
            myofibrilles. Mais contribuent aussi à enrichir les réserves de glycogène et à
            solidifier la structure même des tissus musculaires. Ainsi, chaque répétition et chaque
            haltère soulevé dévoilent une science sophistiquée visant à remodeler et fortifier votre
            corps. Saisir les principes de l'hypertrophie est fondamental pour maximiser
            l'efficacité de vos sessions d'entraînement et pour libérer tout votre potentiel de
            développement musculaire.
          </Text>
          <OrderedList>
            <ListItem>
              <Heading mb="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                Principe de Surcharge Progressive
              </Heading>
              <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
                La clé de l'hypertrophie réside dans la surcharge progressive : soumettre vos
                muscles à un niveau de stress supérieur à celui qu'ils ont déjà connu. Cet objectif
                peut être atteint de diverses manières, telles qu'augmenter le poids soulevé,
                accroître le nombre de répétitions par série, ou intensifier le volume global
                d'entraînement. En défiant constamment vos muscles au-delà de leur confort habituel,
                vous déclenchez un processus d'adaptation naturelle, entraînant une croissance
                musculaire notable.
              </Text>
            </ListItem>
            <ListItem>
              <Heading mb="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                Équilibre entre Volume et Intensité d'Entraînement
              </Heading>
              <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
                Le volume d'entraînement, défini comme le nombre total de répétitions multipliées
                par le poids soulevé, et l'intensité, représentée par le pourcentage de votre
                répétition maximale (1RM) que vous utilisez, sont deux piliers fondamentaux de
                l'hypertrophie. Trouver le juste milieu entre ces éléments est crucial : il s'agit
                de stimuler suffisamment le muscle pour induire une croissance, sans pour autant
                tomber dans le piège du surentraînement ou risquer des blessures. Une programmation
                judicieuse et attentive est donc indispensable pour favoriser une progression
                durable et sécuritaire.
              </Text>
            </ListItem>
            <ListItem>
              <Heading mb="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                Nutrition et Récupération
              </Heading>
              <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
                La croissance musculaire ne se produit pas pendant l'entraînement mais durant les
                périodes de repos. Une récupération adéquate, incluant un sommeil suffisant et une
                nutrition optimisée, est essentielle. Assurez-vous d'obtenir suffisamment de sommeil
                chaque nuit et d'intégrer des jours de repos actifs ou complets dans votre programme
                d'entraînement pour permettre une récupération optimale. Sur le plan nutritionnel,
                les protéines sont au cœur de la réparation et de la construction musculaire. Elles
                fournissent les acides aminés nécessaires pour reconstruire les fibres musculaires
                sollicitées durant l'entraînement. Les glucides, quant à eux, sont indispensables
                pour recharger les réserves d'énergie épuisées par les exercices intensifs.
                N'oublions pas les graisses saines, qui jouent un rôle primordial dans la régulation
                hormonale, notamment pour la production de testostérone, essentielle à la croissance
                musculaire. Adopter une alimentation riche en aliments complets et équilibrée est
                fondamental.
                <br />
                <Heading my="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                  Voici quelques repères
                </Heading>
                <Box as="span" fontWeight={'bold'}>
                  Protéines
                </Box>
                : Un apport de 1,6 à 2,2 grammes par kilogramme de poids corporel est généralement
                conseillé pour ceux qui visent une hypertrophie maximale.
                <br />
                <Box as="span" fontWeight={'bold'}>
                  Glucides
                </Box>
                : Ils devraient représenter une part significative de votre apport calorique,
                particulièrement en période d'entraînement intensif.
                <br />
                <Box as="span" fontWeight={'bold'}>
                  Lipides
                </Box>
                : L'importance des lipides sains ne doit pas être sous-estimée. En plus de leur rôle
                dans la production d'hormones anabolisantes, ils contribuent à la santé globale et
                au bien-être.
                <Box my={'2rem'} />
                En mettant l'accent sur ces éléments nutritionnels et en prêtant attention à votre
                récupération, vous créez un environnement propice à la croissance musculaire et à
                l'amélioration de vos performances.
              </Text>
            </ListItem>
            <ListItem>
              <Heading mb="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                Technique et Forme
              </Heading>
              <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
                La technique et la forme d'exécution des exercices sont les fondations d'un
                entraînement efficace. Une exécution précise et contrôlée est cruciale non seulement
                pour cibler correctement les groupes musculaires souhaités, mais aussi pour
                minimiser les risques de blessures. Assurez-vous que chaque mouvement soit réalisé
                avec une attention particulière à la forme, afin de maximiser le stress appliqué sur
                le muscle ciblé et d'optimiser les gains d'hypertrophie.
              </Text>
              <Heading my="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                L'importance des Compléments Alimentaires
              </Heading>
              <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
                Certains peuvent soutenir votre parcours de musculation. Par exemple, le{' '}
                <Link
                  color={theme.colors.revilal_blue}
                  href="https://labz-nutrition.fr/products/collagen-pro%C2%AE-540g-1?keyword=collagene"
                >
                  collagène
                </Link>{' '}
                peut aider à la santé des articulations, le{' '}
                <Link
                  color={theme.colors.revilal_blue}
                  href="https://labz-nutrition.fr/products/huile-de-cbd-10-labz-nutrition?keyword=cbd"
                >
                  CBD
                </Link>{' '}
                à la récupération, et le{' '}
                <Link
                  color={theme.colors.revilal_blue}
                  href="https://labz-nutrition.fr/products/meta-magnesium-labz-nutrition-100caps-1?keyword=magn%C3%A9sium
                  "
                >
                  magnésium
                </Link>{' '}
                à la fonction musculaire. Pour une exploration plus approfondie des compléments et
                de leurs bienfaits, n'hésitez pas à consulter nos articles dédiés sur le sujet.
              </Text>
            </ListItem>
            <ListItem>
              <Heading mb="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                Hydratation
              </Heading>
              <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
                N'oubliez pas l'importance cruciale de l'hydratation. L'eau est indispensable à
                presque toutes les fonctions corporelles, facilitant le transport des nutriments
                vers les muscles et l'élimination des déchets métaboliques. En parallèle, certains
                suppléments comme la créatine ont démontré leur efficacité pour booster la
                performance lors des entraînements en résistance et soutenir l'hypertrophie.
              </Text>
            </ListItem>
            <ListItem>
              <Heading mb="2rem" fontSize="lg" fontFamily={theme.fonts.body} as={'h4'}>
                Technique, Forme et Préparation
              </Heading>
              <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
                Une technique irréprochable est essentielle pour cibler les muscles efficacement et
                réduire le risque de blessures. Une bonne forme garantit une application optimale du
                stress sur le muscle ciblé. De plus, un échauffement soigné et le maintien d'une
                bonne flexibilité sont indispensables pour préparer le corps à l'effort, améliorer
                la performance et minimiser les risques de blessures.
              </Text>
            </ListItem>
          </OrderedList>

          <Heading mb="4rem" as={'h3'}>
            Conclusion
          </Heading>
          <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
            Atteindre l'hypertrophie est un voyage qui requiert plus qu'un simple effort physique;
            c'est l'harmonie de plusieurs facteurs clés, incluant un entraînement rigoureux, une
            nutrition adéquate, un repos suffisant et une récupération attentive. La patience et la
            cohérence dans votre approche sont cruciales. Si vous cherchez à maximiser vos résultats
            et à bénéficier d'un accompagnement personnalisé, découvrez le programme de suivi à
            distance proposé par VinzerFit. Ensemble, franchissez les étapes de votre transformation
            physique et atteignez vos objectifs de musculation.
          </Text>
        </Box>
      </Box>
    </>
  );
};
