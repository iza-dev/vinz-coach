import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardProps,
  Flex,
  Heading,
  Text,
  useTheme
} from '@chakra-ui/react';
import { FC } from 'react';
import { FaLongArrowAltRight } from 'react-icons/fa';

type Article = {
  title: string;
  subtitle?: string;
  description: string;
};

type ArticleCardProps = {
  data: Article;
  image: JSX.Element;
};

export const ArticleCard: FC<ArticleCardProps & CardProps> = ({ data, image, ...rest }) => {
  const theme = useTheme();
  return (
    <Card {...rest}>
      <CardHeader>
        <Flex position={'relative'}>{image}</Flex>
      </CardHeader>
      <CardBody display={'flex'} flexDirection={'column'} justifyContent={'space-between'}>
        <Heading as={'h1'}>{data.title}</Heading>
        <Heading as={'h2'} fontFamily={theme.fonts.p_intro} fontSize={'lg'}>
          {data.subtitle}
        </Heading>
        <Text maxWidth={'md'} isTruncated>
          {data.description}
        </Text>
        <Button
          aria-label="bouton pour lire la suite d'un article"
          rightIcon={<FaLongArrowAltRight />}
        >
          Lire la suite
        </Button>
      </CardBody>
    </Card>
  );
};
