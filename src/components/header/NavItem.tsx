import { Box, Link, Menu, MenuButton, MenuList, useColorModeValue } from '@chakra-ui/react';
import { FC, ReactNode } from 'react';

type NavItemProps = {
  label: string;
  href?: string;
  children?: ReactNode;
};

export const NavItem: FC<NavItemProps> = ({ label, href, children }) => {
  const isDropdown = children !== undefined;
  const hoverBgColor = useColorModeValue('gray.200', 'gray.700');

  return (
    <Box>
      {isDropdown ? (
        <Menu>
          <MenuButton
            as={Link}
            py={2}
            px={4}
            href={href ?? '#'}
            rounded={'md'}
            _hover={{
              textDecoration: 'none',
              bg: hoverBgColor
            }}
            style={{ whiteSpace: 'nowrap' }}
            fontSize={{ base: '5vmin', md: '3.5vmin', lg: '2vmin' }}
          >
            {label}
          </MenuButton>
          <MenuList marginX={'1rem'} fontSize={{ base: '5vmin', md: '3.5vmin', lg: '2vmin' }}>
            {children}
          </MenuList>
        </Menu>
      ) : (
        <Link
          px={4}
          py={2}
          rounded={'md'}
          _hover={{
            textDecoration: 'none',
            bg: hoverBgColor
          }}
          href={href}
          style={{ whiteSpace: 'nowrap' }}
          fontSize={{ base: '5vmin', md: '3.5vmin', lg: '2vmin' }}
        >
          {label}
        </Link>
      )}
    </Box>
  );
};
