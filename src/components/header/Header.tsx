import { Flex, Icon, IconButton, Image } from '@chakra-ui/react';
import { FC, useState } from 'react';
import { FaBars, FaTimes } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { MenuNav } from './MenuNav';
import { Navbar, NavbarToggle } from './Navbar';

import logo from '../../assets/images/logos/logo_vinzerfit.webp';

const header = {
  as: 'header',
  color: 'black',
  position: 'fixed',
  left: '50%',
  transform: 'translateX(-50%)',
  px: '2rem',
  py: '1rem',
  zIndex: '100',
  backgroundColor: 'white',
  justifyContent: 'space-between',
  borderRadius: 'md'
};

export const Header: FC = () => {
  const [isToggle, setIsToggle] = useState(false);

  const toggleMenu = () => {
    setIsToggle(!isToggle);
  };

  return (
    <Flex
      alignItems={!isToggle ? 'center' : 'none'}
      sx={header}
      height={isToggle ? '100vh' : { md: '5rem' }}
      top={isToggle ? 0 : '2rem'}
      width={{ base: isToggle ? '100vw' : '90vw', md: isToggle ? '100vw' : '85vw', lg: '70vw' }}
    >
      <Link to={'/'}>
        <Image src={logo} minHeight={'3rem'} maxHeight={'4rem'} width={'auto'} />
      </Link>
      {!isToggle && (
        <>
          <Navbar>
            <MenuNav />
          </Navbar>
          <IconButton
            aria-label="Open Menu"
            icon={<Icon as={FaBars} />}
            display={{ md: 'flex', lg: 'none' }}
            marginY={'auto'}
            onClick={toggleMenu}
          />
        </>
      )}

      {isToggle && (
        <>
          <NavbarToggle>
            <MenuNav />
          </NavbarToggle>
          <IconButton
            aria-label="Close Menu"
            icon={<Icon as={FaTimes} />}
            display={{ md: 'flex', lg: 'none' }}
            onClick={toggleMenu}
          />
        </>
      )}
    </Flex>
  );
};
