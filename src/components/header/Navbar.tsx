import { Flex, FlexProps } from '@chakra-ui/react';
import { FC } from 'react';

const navbar = {
  as: 'nav',
  alignItems: { base: 'flex-start', lg: 'center' }
};

export const Navbar: FC<FlexProps> = ({ children, ...rest }) => {
  return (
    <Flex {...rest} sx={navbar} flexDirection={'row'} display={{ base: 'none', lg: 'flex' }}>
      {children}
    </Flex>
  );
};

export const NavbarToggle: FC<FlexProps> = ({ children, ...rest }) => {
  return (
    <Flex {...rest} sx={navbar} flexDirection={'column'} marginY={'auto'} gap={'2rem'}>
      {children}
    </Flex>
  );
};
