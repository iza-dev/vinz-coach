import { MenuItem } from '@chakra-ui/react';
import { FC } from 'react';
import { Link } from 'react-router-dom';
import { NavItem } from './NavItem';

export const MenuNav: FC = () => {
  return (
    <>
      <NavItem label="Acceuil" href="/" />
      <NavItem label="A propos" href="#" />
      <NavItem label="Coaching & Suivi " href="/programs">
        <MenuItem as={Link} to="#">
          Force et Liberté
        </MenuItem>
        <MenuItem as={Link} to="#">
          Bien-être et Équilibre
        </MenuItem>
        <MenuItem as={Link} to="#">
          Road to Titan
        </MenuItem>
        <MenuItem as={Link} to="#">
          Road to Godness
        </MenuItem>
        <MenuItem as={Link} to="#">
          Road to Colmar
        </MenuItem>
      </NavItem>
      <NavItem label="Formations" href="#">
        <MenuItem as={Link} to="#">
          Posing
        </MenuItem>
      </NavItem>
      <NavItem label="Transformations" href="#" />
      <NavItem label="Articles" href="#">
        <MenuItem as={Link} to="#">
          Nutrition
        </MenuItem>
        <MenuItem as={Link} to="/articles/entraînements-et-techniques">
          Entraînements et techniques
        </MenuItem>
        <MenuItem as={Link} to="#">
          Récupération
        </MenuItem>
        <MenuItem as={Link} to="#">
          Santé et bien-être
        </MenuItem>
        <MenuItem as={Link} to="#">
          Équipements et accessoires
        </MenuItem>
      </NavItem>
    </>
  );
};
