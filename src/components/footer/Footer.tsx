import { Divider, Flex, FlexProps, Heading, Input, Text } from '@chakra-ui/react';
import { FC } from 'react';

export const Footer: FC<FlexProps> = ({ ...rest }) => {
  return (
    <Flex
      {...rest}
      p={'1rem'}
      direction={'column'}
      position={'relative'}
      width={'100%'}
      marginTop={{ base: '5rem', md: '8rem', lg: '15rem' }}
    >
      <Flex justifyContent={'space-around'} direction={{ base: 'column', md: 'row' }} gap={'2rem'}>
        <Flex direction={'column'} maxWidth={'xs'}>
          <Heading>Contactez-moi</Heading>
          <Text>contact@vinzerfit.com</Text>
        </Flex>
        <Flex direction={'column'} maxWidth={'xs'}>
          <Heading>Newsletter</Heading>
          <Text>
            Découvrez en avant-première mes astuces exclusives, les dernières nouveautés, offres
            spéciales, et bien d'autres surprises, en vous abonnant à la newsletter !
          </Text>
          <Input htmlSize={4} width="auto" placeholder="Email ici" />
        </Flex>
      </Flex>
      <Divider marginY={'1rem'} />
      <Flex gap={'2rem'} flexWrap="wrap" marginX={'auto'} p={'2rem'}>
        <Text flexShrink={0}>Mentions légales</Text>
        <Text flexShrink={0}>Gestion des cookies</Text>
        <Text flexShrink={0}>copyright© 2024</Text>
      </Flex>
    </Flex>
  );
};
