import { Grid, GridItem, GridProps, ListItem } from '@chakra-ui/react';

type ListItem = {
  id: number;
  name?: string;
};

type ListProps<T extends ListItem> = {
  items: T[];
  renderItem: (item: T) => JSX.Element;
} & GridProps;

export const ListG = <T extends ListItem>({ items, renderItem, ...rest }: ListProps<T>) => {
  return (
    <Grid
      templateColumns={{
        md: 'repeat(auto-fill, minmax(250px, 1fr) )',
        lg: 'repeat(auto-fill, minmax(400px, 1fr) )'
      }}
      rowGap={'0.5rem'}
      columnGap={'0.5rem'}
      justifyItems={'center'}
      marginX={'auto'}
      marginTop={'0.5rem'}
      {...rest}
    >
      {items.map(item => (
        <GridItem key={item.id}>{renderItem(item)}</GridItem>
      ))}
    </Grid>
  );
};
