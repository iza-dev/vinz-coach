import { Box, BoxProps } from '@chakra-ui/react';
import { FC } from 'react';

export const Section: FC<BoxProps> = ({ children, ...rest }) => {
  return (
    <Box {...rest} as={'section'} p={{ base: 'auto', md: '5rem' }} minHeight={'100vh'}>
      {children}
    </Box>
  );
};
