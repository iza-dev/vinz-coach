import { ImageProps } from '@chakra-ui/react';
import { FC } from 'react';

type ImageOptimizedProps = {
  path: { webp: string; png: string };
};

export const ImageOptimized: FC<ImageProps & ImageOptimizedProps> = ({ path, ...rest }) => {
  return (
    <picture>
      <source srcSet={path.png} type="image/png" />
      <source srcSet={path.webp} type="image/webp" />
      <img src={path.webp} alt={rest.alt} />
    </picture>
  );
};
