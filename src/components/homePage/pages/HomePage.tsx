import { FC } from 'react';

import {
  AspectRatio,
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  Image,
  Link,
  Text,
  useTheme
} from '@chakra-ui/react';
import { ArticleCard } from '../../articles';
import { ListG } from '../../commons';
import { BigPicture } from '../BigPicture';

import programs from '../../../data/programs.json';

import pose1 from '../../../assets/images/formations/posing/pose1.webp';
import pose2 from '../../../assets/images/formations/posing/pose2.webp';
import pose3 from '../../../assets/images/formations/posing/pose3.webp';
import pose4 from '../../../assets/images/formations/posing/pose4.webp';

import img_victory_kevin from '../../../assets/images/articles/victory_kevin.png';
import img_victory_theo from '../../../assets/images/articles/victory_theo.png';

import colmar_vinz from '../../../assets/images/programs/colmar_vinz.webp';
import freedom_woman from '../../../assets/images/programs/freedom_woman.webp';
import strength_man from '../../../assets/images/programs/strength_man.webp';
import titan_man from '../../../assets/images/programs/titan_man.webp';
import titan_woman from '../../../assets/images/programs/titan_woman.webp';

export const HomePage: FC = () => {
  const imgProgramms = [strength_man, freedom_woman, titan_man, titan_woman, colmar_vinz];
  const theme = useTheme();

  return (
    <Box>
      <BigPicture />
      <Box as="section" aria-label="section des programmes" marginTop={'5rem'}>
        <Box
          marginX={'auto'}
          marginTop={'5rem'}
          marginBottom={'2rem'}
          width={theme.sizes.textWidth}
        >
          <Heading textAlign={'center'} fontSize={'15vmin'}>
            Coaching
          </Heading>
          <Text fontSize={{ md: '1.25rem' }}>
            Adopte une routine sur mesure, qui t'aider à transformer ton corps.
            <br />
            Tu es déterminé à prendre ton physique en main? L'excellence dans la transformation
            corporelle ne s'improvise pas. Elle demande discipline, méthode, et le soutien d'un
            expert pleinement engagé dans ton succès.
          </Text>
        </Box>
        <ListG
          items={programs.data}
          renderItem={item => (
            <Box position={'relative'} display="flex" justifyContent="start" alignItems="end">
              <Box zIndex={10} position={'absolute'} p={'1rem'}>
                <Heading>{item.name}</Heading>
                <Divider />
                <Text fontSize={'1.5rem'}>{`${item.duration_months} mois`}</Text>
              </Box>
              <Link href={`/programs/${item.id}`}>
                <Box overflow="hidden">
                  <Image
                    src={imgProgramms[item.id - 1]}
                    transition="transform 0.3s ease-in-out"
                    _hover={{
                      transform: 'scale(1.1)'
                    }}
                    filter={'brightness(50%);'}
                  />
                </Box>
              </Link>
            </Box>
          )}
        />
      </Box>
      <Box as="section" aria-label="section des formations" marginTop={'5rem'}>
        <Box marginX={'auto'} marginBottom={'2rem'} width={theme.sizes.textWidth}>
          <Heading textAlign={'center'} fontSize={'15vmin'}>
            Formations
          </Heading>
          <Text fontSize={{ md: '1.25rem' }}>
            Si tu souhaites transformer tes habitudes, professionnaliser ta routine, et élève ton
            style de vie au niveau supérieur. Tu es au bon endroit! Cette sélection de cours vidéo,
            a été spécifiquement créés pour te guider et te stimuler chaque jour. Ici, tu trouveras
            tout le nécessaire pour avancer vers tes objectifs, étape par étape. Laisse-moi
            t'accompagner vers la meilleure version de toi-même.
          </Text>
        </Box>
        <Flex
          backgroundColor={'white'}
          height={{ base: '100vh', md: '70vh', lg: '450px' }}
          width={'80vw'}
          marginX={'auto'}
          flexDirection={{ base: 'column', md: 'column', lg: 'row' }}
        >
          <Box
            position={'relative'}
            height={'auto'}
            p={'2rem'}
            backgroundColor={'red.500'}
            flexBasis={'33.33%'}
            clipPath={{ base: 'none', lg: 'polygon(0% 0%, 75% 0%, 100% 50%, 75% 100%, 0% 100%)' }}
          >
            <Box
              width={'100%'}
              top={{ base: '25%', lg: '40%' }}
              left={'50%'}
              transform={{ base: 'translate(-50%,-25%)', lg: 'translate(-50%,-40%)' }}
              position={'absolute'}
              // backgroundColor={'gray.600'}
              textAlign={'center'}
            >
              <Heading>Posing</Heading>
              <Text fontSize={{ base: '1.25rem', md: '1.5rem', lg: '3vmin' }}>
                Pose comme un pro
              </Text>
            </Box>
            <Divider
              top={'65%'}
              width={'70%'}
              left={'50%'}
              transform={'translate(-50%,-65%)'}
              position={'absolute'}
            />
            <Button
              top={{ base: '90%', lg: '85%' }}
              left={'50%'}
              transform={{
                base: 'translate(-50%,-90%)',

                lg: 'translate(-50%,-85%)'
              }}
              position={'absolute'}
            >
              En savoir plus
            </Button>
          </Box>
          <Box p={'2rem'} height={'auto'} flexBasis={'66.67%'}>
            <Flex
              flexWrap={'wrap'}
              justifyContent={{ base: 'center', lg: 'flex-end' }}
              columnGap={{ base: '0.5rem', md: '9rem', lg: '15rem' }}
              rowGap={{ base: '4rem', lg: '3rem' }}
              width={{ lg: '80%' }}
              height={'100%'}
              maxHeight={'350px'}
            >
              <Image src={pose1} width={{ base: '35%', md: '6rem', lg: 'auto' }} />
              <Image src={pose2} width={{ base: '35%', md: '6rem', lg: 'auto' }} />
              <Image src={pose3} width={{ base: '35%', md: '6rem', lg: 'auto' }} />
              <Image src={pose4} width={{ base: '35%', md: '6rem', lg: 'auto' }} />
            </Flex>
          </Box>
        </Flex>
      </Box>

      <Box as="section" aria-label="section interviews" marginTop={'5rem'}>
        <Flex direction={'column'} padding={'2rem'} minHeight={'100vh'}>
          <Box marginX={'auto'} marginBottom={'2rem'} width={theme.sizes.textWidth}>
            <Heading textAlign={'center'} fontSize={'15vmin'}>
              Interview
            </Heading>
            <Text fontSize={{ md: '1.25rem' }}>
              Entretien exclusif avec{' '}
              <Link
                color={theme.colors.revilal_blue}
                href="https://www.youtube.com/channel/UCxWnUY4dcrh3p0nOWXHCFDQ"
              >
                The ROB
              </Link>
              : débat sur le dopage.{' '}
              <span style={{ fontWeight: 'bold' }}>
                Comment atteindre l'excellence tout en préservant l'intégrité de ses principes
                naturels?
              </span>
            </Text>
          </Box>
          <AspectRatio maxWidth={'3xl'} ratio={1.5} left={{ base: 0, lg: '25vw' }}>
            <iframe
              src="https://www.youtube.com/embed/NyX-Zboa6vc?si=p4jT5iGew4CZKWeT"
              title="YouTube video player"
              allowFullScreen
            />
          </AspectRatio>
        </Flex>
      </Box>

      <Box
        as="section"
        aria-label="section actualités"
        marginTop={{ base: '-2rem', lg: '5rem' }}
        height={'auto'}
      >
        <Heading textAlign={'center'} fontSize={'15vmin'}>
          Actualités
        </Heading>
        <ListG
          width={'70vw'}
          templateColumns={{
            md: 'repeat(2, 1fr )',
            lg: 'repeat(3, 1fr )'
          }}
          items={[
            {
              id: 1,
              title: 'Au Cœur du Succès',
              subtitle: 'de @kev_crd21 pour sa première compétition',
              description:
                "À seulement 22 ans, @kev_crd21 s'est imposé comme champion de France junior et a obtenu sa qualification pour la Diamond Cup en se classant 5ème dans la catégorie Men’s Physique. Une performance remarquable, qui témoigne de l'excellence et de l'efficacité de l'accompagnement sur mesure offert par Vinzerfit. Un coach, une vision, un champion.Rejoignez-nous pour une immersion dans le récit inspirant de Kevin et explorez le programme d'entraînement rigoureux et la préparation mentale stratégique orchestrés par Vincent Cuesta. Découvrez les secrets d'une préparation gagnante qui façonne les champions.",
              image: img_victory_kevin
            },
            {
              id: 2,
              title: 'Théo Di Biagio et Vincent Cuesta',
              subtitle: 'un Duo Gagnant au Top de Colmar en Men’s Physique 2023',
              description:
                "Dans l'arène exigeante du Top de Colmar, Théo Di Biagio a brillamment décroché la 3ème place en Men’s Physique. Soutenu par l'expertise de son coach,  Vinzerfit. Sa silhouette parfaitement ciselée, est le fruit d'un entraînement rigoureux et d'une préparation méticuleuse.Immergez-vous dans le parcours  de cet athlète, et découvrez comment la méthodologie unique de  Vinzerfit sculpte les champions de demain.",
              image: img_victory_theo
            }
          ]}
          renderItem={item => (
            <ArticleCard
              minHeight={'100%'}
              width={'17rem'}
              data={item}
              image={<Image margin={'auto'} src={item.image} alt={'test'} borderRadius={'lg'} />}
            />
          )}
        />
      </Box>

      {/* <section aria-label={'section tendances fitness'}>
        <Flex direction={'column'} padding={'2rem'} minHeight={'100vh'}>
          <Heading as={'h1'} fontSize={{ base: '5xl', md: '6xl', lg: '8xl' }} pb={'4rem'}>
            Tendances Fitness
          </Heading>
          <Text fontSize="lg" mb="4rem" textAlign={'justify'} maxWidth={'3xl'}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita sequi reprehenderit
            eos rerum nesciunt placeat vitae culpa aspernatur adipisci cupiditate?
          </Text>
          <ListG
            zIndex={-1}
            justifyContent={'flex-end'}
            gap={'1rem'}
            // TODO add real datas
            items={[
              {
                id: 1,
                title: 'Tendance 1',
                description:
                  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut hic tempore soluta vitae sed totam earum!',
                image: img_p1
              },
              {
                id: 2,
                title: 'Tendance 2',
                description:
                  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut hic tempore soluta vitae sed totam earum!',
                image: img_p2
              },
              {
                id: 3,
                title: 'Tendance 3',
                description:
                  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut hic tempore soluta vitae sed totam earum!',
                image: img_p3
              }
            ]}
            renderItem={item => (
              <ArticleCard
                data={item}
                image={
                  <Image
                    margin={'auto'}
                    maxWidth={{ base: '2xs', md: '2xl', lg: 'md' }}
                    src={item.image}
                    alt={'test'}
                    borderRadius={'lg'}
                  />
                }
              />
            )}
          />
        </Flex>
      </section> */}
      {/* <section aria-label={'section partenaires'}>
        <Box padding={'2rem'}>
          <Heading as={'h1'} fontSize={{ base: '5xl', md: '6xl', lg: '8xl' }} pb={'4rem'}>
            Partenaires
          </Heading>
          <Flex direction={isSmartphone ? 'column' : 'row'} justifyContent={'space-around'}>
            <Image src={logo_labz} alt="logo labz" />
            <Image src={logo_evrest} alt="logo evrest" />
          </Flex>
        </Box>
      </section> */}
    </Box>
  );
};
