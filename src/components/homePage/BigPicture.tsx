import { AspectRatio, Button, Heading, Image } from '@chakra-ui/react';
import { FC } from 'react';
import vinz from '../../assets/images/vinz_big_picture.webp';
import { Section } from '../commons';

const image = {
  left: { base: '50%', md: '35vw', lg: '60vw' },
  transform: { base: 'translateX(-50%)', md: 'auto' },
  overflow: 'hidden',
  zIndex: 5
};

const heading = {
  maxWidth: { base: '82.5vmin', md: '50vmin', lg: '60vmin' },
  p: { base: '1rem', md: '1.5rem', lg: '2rem' },
  position: 'absolute',
  top: { base: '8rem', md: '10rem', lg: '9rem' },
  left: { md: '2rem', lg: '10vw' },
  fontSize: { base: '15vmin', md: '12vmin', lg: '15vmin' }
};

const button = {
  minWidth: { base: '2xs', md: '50vmin', lg: '60vmin' },
  minHeight: '7vmin',
  backgroundColor: 'red.500',
  color: { base: 'gray.200', md: 'gray.200' },
  position: 'absolute',
  top: { base: '80vh', md: '40rem', lg: '80vmin' },
  left: { base: '50%', md: '4rem', lg: '10vw' },
  transform: { base: 'translateX(-50%)', md: 'auto' },
  fontSize: { base: '6vmin', md: '4vmin' },
  zIndex: '10'
};

export const BigPicture: FC = () => {
  const isLargeScreen = window.innerWidth >= 1920;
  return (
    <Section overflow={'hidden'} position={'relative'}>
      <AspectRatio
        sx={image}
        ratio={{ base: 10 / 16, md: 2 / 3, lg: isLargeScreen ? 19 / 28 : 12 / 18 }}
        top={{ base: '40vh', md: '45vh', lg: '15vh' }}
        maxW={{ base: 'md', md: 'md', lg: isLargeScreen ? '3xl' : 'xl' }}
      >
        <Image src={vinz} />
      </AspectRatio>

      <Heading sx={heading}>Level up your lifestyle</Heading>
      <Button sx={button}>Join</Button>
    </Section>
  );
};
